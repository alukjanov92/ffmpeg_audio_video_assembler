<?php

include __DIR__ . "/php/randomName.php";
require_once __DIR__ . "/vendor/autoload.php";
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class VideoQueuePlanner {

  public function registerTask ($videoType, $requestParams) {
    if ($videoType === "text") {
      $params = $requestParams->extractTextParams();
    } else {
      $params = $requestParams->extractImageParams();
    }

    $data = [
      "videoType" => $videoType,
      "params" => $params
    ];

    $this->publishMessage($data);
  }

  public function publishMessage ($data, $shouldEncode = true) {
    $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
    $channel = $connection->channel();

    $channel->queue_declare("video_compilation_queue", false, false, false, false);

    $message = new AMQPMessage($shouldEncode ? json_encode($data) : $data);
    $channel->basic_publish($message, "", "video_compilation_queue");

    $channel->close();
    $connection->close();
  }

}
