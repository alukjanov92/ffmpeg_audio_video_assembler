<?php
ini_set("include_path", __DIR__."/php/");
define("ROOT_PATH", __DIR__."/");
include "siteApi/SiteApiHelper.php";
include "RequestParams.php";
include "ConfigParams.php";
include "ParamsValidator.php";
include "siteApi/NamesList.php";

$requestParams = new RequestParams();
$configParams = new configParams();
$paramsValidator = new ParamsValidator();
$siteApiHelper = new SiteApiHelper($configParams);
try{
  $paramsValidator->validateNamesRequest($requestParams);
  $video_type = $requestParams->getVideoType();
  $namesList = new NamesList();
  $list = $namesList->getSupportedNamesList($video_type);
  $siteApiHelper->sendNamesList(['names_list' => $list]);
}catch(Exception $e){
  $code = $e->getCode();
  $msg = $e->getMessage();
  $siteApiHelper->declineRequest($msg, $code);
}
?>
