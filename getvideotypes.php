<?php
ini_set("include_path", __DIR__."/php/");
define("ROOT_PATH", __DIR__."/");
include "ConfigParams.php";

function getVideosList () { //@TODO
  return [
    "dedmoroz" => [
      "width" => 230,
      "height" => 230
    ],
    "transformer" => [
      "width" => 921,
      "height" => 921
    ],
    "feya" => [
      "width" => 342,
      "height" => 342
    ],
    "clown" => [
      "width" => 555,
      "height" => 555
    ]
  ];
}

include "siteApi/SiteApiHelper.php";

try {
  $configParams = new ConfigParams();
  $siteApiHelper = new SiteApiHelper($configParams);
  $siteApiHelper->sendVideoTypes(getVideosList());
} catch (Exception $e) {
  $code = $e->getCode();
  $msg = $e->getMessage();
  $siteApiHelper->declineRequest($msg, $code);
}
