<?php
ini_set("include_path", __DIR__."/php/");
define("ROOT_PATH", __DIR__."/");
define("TMP", ROOT_PATH . "tmp/");
define("DEBUG", false);
define("DEBUG_VIDEO", false);
define("DEBUG_FTP", false);
define("USE_AMAZON", true);


include "siteApi/SiteApiHelper.php";
include "db/OperationLogger.php";
include "ConfigParams.php";
include "RequestParams.php";
include "ParamsValidator.php";
include "logger.php";
include "queue_planner.php";

function handleRequestAndRegisterTask ($type) {

  $requestParams = new RequestParams();
  $configParams = new ConfigParams();
  $siteApiHelper = new SiteApiHelper($configParams);
  $loger = new Logger();
  $operationLogger = new OperationLogger();
  $paramsValidator = new ParamsValidator();
  $token = $requestParams->getToken();

  try {
    if ($type === "text") {
      $paramsValidator->validateTextVideoRequest($requestParams, $operationLogger);
    } else if ($type === "image") {
      $paramsValidator->validateVideoRequest($requestParams, $operationLogger);
    }
    $operationLogger->registerOperation($token);
    $siteApiHelper->acceptRequest(['status'=>'accepted', 'token' => $token]);

    $queuePlanner = new VideoQueuePlanner();
    $queuePlanner->registerTask($type, $requestParams);

  } catch (Exception $e) {
    $msg = $e->getMessage();
    $code = $e->getCode();
    $logPath = null;
    if($e instanceof ParamsException){
      $siteApiHelper->declineRequest($msg, $code);
    }elseif($e instanceof FtpException){
      $operationLogger->updateOperationFail($token, $msg, $code);
      $siteApiHelper->sendOperationFail($token, $msg, $code);
    }elseif($e instanceof DbException){
      $logPath = "db_errors.log";
    }else{
      $operationLogger->updateOperationFail($token);
    }

    $loger->logException($token, $e, $logPath);
  }

}
