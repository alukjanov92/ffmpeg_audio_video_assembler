<?php
ini_set("include_path", __DIR__."/php/");
define("ROOT_PATH", __DIR__."/");
define("TMP", ROOT_PATH . "tmp/");
include "siteApi/SiteApiHelper.php";
include "Params.php";
include "db/OperationLogger.php";
$errorCodes = include "config/errorCodes.php";
$params = new Params();
$siteApiHelper = new SiteApiHelper($params);
$opLogger = new OperationLogger();
try{
  $params->checkToken();
  $token = $params->getToken();
  $statusInfo = $opLogger->getOperationStatus($token);
  if($statusInfo){
    $status = $statusInfo['status'];
    $info = $statusInfo['info'];
    $code = $statusInfo['code'];
    $siteApiHelper->sendOperationCurrentStatus($token, $status, $code, $info);
  }else{
    http_response_code(400);
    $siteApiHelper->sendOperationCurrentStatus($token, 'not_found', 23, $errorCodes["db"]["23"]);
  }
}catch(Exception $e){
  if($e instanceof ParamsException){
    $msg = $e->getMessage();
    $code = $e->getCode();
    $siteApiHelper->declineRequest($token, $msg, $code);
  }
}
?>
