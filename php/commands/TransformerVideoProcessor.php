<?php

include_once "BaseVideoCommand.php";
include_once "Videoparams/TransformerParams.php";

class TransformerVideoProcessor extends BaseVideoCommand {
  public function __construct($requestParams) {
    parent::__construct($requestParams);
    $commandParams = new TransformerParams();
    $this->commandParams = [
      "audio_params" => $commandParams->getAudioParams($this->requestParams),
      "video_params" => $commandParams->getVideoParams(),
      "photo_params" => $commandParams->getPhotoParams()
    ];
  }

  public function processVideo () {
    $audioChannel = $this->processAudio();
    $videoChannel = $this->applyImageToVideoChannel();
    $resultVideo = $this->applyAudioChannelToVideo($videoChannel, $audioChannel);
    unlink($audioChannel);
    unlink($videoChannel);
    return $resultVideo;
  }

  public function processAudio () {
    $audioParams = $this->commandParams["audio_params"];
    $CONCATED = TMP . generateRandomName() . ".mp3";
    $cmd = "ffmpeg -y -i \"concat:" . $audioParams['full_name_audio_channel'] . "\" -c copy $CONCATED";
    $this->executeCommand($cmd);

    $mixedWithMusic = TMP . generateRandomName() . ".mp3";

    $cmd = "ffmpeg -y -i $CONCATED -i " . $audioParams['music'] . " -filter_complex amix=inputs=2:duration=first $mixedWithMusic";

    $this->executeCommand($cmd);
    unlink($CONCATED);

    return $mixedWithMusic;
  }

  public function applyImageToVideoChannel () {
    $img = $this->requestParams->getPhoto();
    $videoParams = $this->commandParams["video_params"];
    $imageParams = $this->commandParams["photo_params"];

    $INPUT_VIDEO = $videoParams["sample_path"];
    $OVERLAY_X_SHIFT = $imageParams["x"];
    $OVERLAY_Y_SHIFT = $imageParams["y"];
    $OVERLAY_START_SECOND = $imageParams["start_second"];
    $OVERLAY_START_FADE_SECONDS = $imageParams["start_fade_seconds"];
    $OVERLAY_END_SECOND = $imageParams["end_second"];
    $OVERLAY_END_FADE_SECONDS = $imageParams["end_fade_seconds"];
    $START_SECONDS = $imageParams["start_second"];
    $END_SECONDS = $imageParams["end_second"];
    $OUTPUT_FILE_NAME = TMP . generateRandomName() . ".mp4";
    $cmd = "ffmpeg -y -i $INPUT_VIDEO -f image2 -loop 1 -r 24 -i $img -preset ultrafast -filter_complex \"[1:0] format=pix_fmts=yuva420p, fade=t=in:st=$OVERLAY_START_SECOND:d=$OVERLAY_START_FADE_SECONDS:alpha=1, fade=t=out:st=$OVERLAY_END_SECOND:d=$OVERLAY_END_FADE_SECONDS:alpha=1 [ov]; [0:v][ov] overlay=$OVERLAY_X_SHIFT:$OVERLAY_Y_SHIFT:enable='between(t,$START_SECONDS,$END_SECONDS)'\" -t 00:01:45 $OUTPUT_FILE_NAME";
    $this->executeCommand($cmd);
    unlink($img);
    return $OUTPUT_FILE_NAME;
  }

  public function applyAudioChannelToVideo ($videoPath, $audioPath) {
    $resultVideoPath = TMP . generateRandomName() . ".mp4";
    $cmd = "ffmpeg -y -i $videoPath -i $audioPath -map 0 -map 1 -acodec aac -strict -2 -vcodec copy -shortest $resultVideoPath";
    $this->executeCommand($cmd);
    return $resultVideoPath;
  }

}

