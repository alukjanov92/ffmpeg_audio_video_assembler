<?php

include_once "BaseVideoCommand.php";
include_once "Videoparams/ClownParams.php";

class ClownVideoProcessor extends BaseVideoCommand {

  public function __construct ($requestParams) {
    parent::__construct($requestParams);
    $commandParams = new ClownParams();
    $this->commandParams = [
      "audio_params" => $commandParams->getAudioParams($this->requestParams),
      "video_params" => $commandParams->getVideoParams(),
    ];
  }

  public function processVideo () {
    $audioChannel = $this->processAudio();
    $videoChannel = $this->applyImageToVideoChannel();
    $resultVideo = $this->applyAudioChannelToVideo($videoChannel, $audioChannel);
    unlink($audioChannel);
    unlink($videoChannel);
    return $resultVideo;
  }

  public function applyAudioChannelToVideo ($videoPath, $audioPath) {
    $resultVideoPath = TMP . generateRandomName() . ".mp4";
    $cmd = "ffmpeg -y -i $videoPath -i $audioPath -map 0 -map 1 -acodec aac -strict -2 -vcodec copy -shortest $resultVideoPath";
    $this->executeCommand($cmd);
    return $resultVideoPath;
  }

  public function processAudio () {
    $audioParams = $this->commandParams["audio_params"];
    $OUTPUT_FILE_PATH = TMP . generateRandomName() . ".mp3";
    $cmd = "ffmpeg -y -i \"concat:" . $audioParams['full_name_audio_channel'] . "\" -c copy $OUTPUT_FILE_PATH";

    $this->executeCommand($cmd);

    $MIXED_WITH_MUSIC = TMP . generateRandomName() . ".mp3";
    $cmd = "ffmpeg -y -i $OUTPUT_FILE_PATH -i " . $audioParams['music'] . " -filter_complex amix=inputs=2:duration=first $MIXED_WITH_MUSIC";

    $this->executeCommand($cmd);

    unlink($OUTPUT_FILE_PATH);
    return $MIXED_WITH_MUSIC;
  }

  public function applyImageToVideoChannel () {
    $imgVideo = $this->processImageVideo();
    $image = $this->requestParams->getPhoto();
    return $this->overlayWithImage($imgVideo, $image);
  }

  public function processImageVideo () {
    $photoWidthPerFrames = [
      65, 128, 192, 252, 313, 368, 419, 464, 504, 534, 551, 555, 555, 545, 535, 520, 505, 490, 474, 460, 448, 439, 433, 433, 433
    ];

    $photoAnglePerFrames = [
      180, 189, 198, 209, 216, 229, 243, 250, 260, 270, 280, 290, 300, 309, 317, 325, 333, 339, 345, 350, 354, 357, 359.5, 360
    ];

    $framesDirPath = TMP . generateRandomName() . "/";
    $resultVideo = TMP . generateRandomName() . ".mov";
    $img = $this->requestParams->getPhoto();
    mkdir($framesDirPath);

    foreach ($photoWidthPerFrames as $key => $value) {
      $name = "frame_$key.png";
      $path = $framesDirPath . $name;
      $degree = $photoAnglePerFrames[$key];
      if (!$degree) {
        $degree = 0;
      }
      $degree = is_null($degree) ? 0 : $degree;
      $cmd = "convert $img -background 'rgba(0,0,0,0)' -gravity center -resize $value"."x"."$value^ -gravity center -rotate $degree -extent 770x770 $path";
      $this->executeCommand($cmd);
    }

    $cmd = "ffmpeg -y -i $framesDirPath/frame_%d.png -vcodec copy -framerate 25 $resultVideo";
    $this->executeCommand($cmd);
    rmdir($framesDirPath);

    return $resultVideo;
  }

  public function overlayWithImage ($imgVideo, $IMG) {
    $RESULT_VIDEO = TMP . generateRandomName() . ".mp4";
    $SAMPLE = $this->commandParams["video_params"]["sample_path"];
    $cmd = <<<EOT
    ffmpeg -y -i $SAMPLE -itsoffset 00:00:49.16 -i $imgVideo -i $IMG -filter_complex "format=yuv420p, \
      overlay=\
        eval='frame'\
        :x=if(\
          between(\
            n\, 1229\, 1232\
          )\,\
          1018\,\
          if(\
           between(\
            n\, 1233\, 1234\
           )\,\
           1012\,\
           if(\
            between(\
              n\, 1235\, 1235\
            )\,\
            1010\,\
            if(\
              between(\
                n\, 1236\, 1237\
              )\,\
              1008\,\
              if(\
                between(\
                  n\, 1238\, 1238\
                )\,\
                1007\,\
                if(\
                  between(\
                    n\, 1239\, 1239\
                  )\,\
                  1005\,\
                  if(\
                    between(\
                      n\, 1240\, 1241\
                    )\,\
                    1006\,\
                    if(\
                      between(\
                        n\, 1242\, 1243\
                      )\,\
                      1008\,\
                      if(\
                        between(\
                          n\, 1244\, 1245\
                        )\,\
                        1013\,\
                        if(\
                          between(\
                            n\, 1246\, 1247\
                          )\,\
                          1015\,\
                          if(\
                            between(\
                              n\, 1248\, 1248\
                            )\,\
                            1017\,\
                            1018\
                          )\
                        )\
                      )\
                    )\
                  )\
                )\
              )\
            )\
           )\
          )\
        )\
        :y=if(\
          between(n\, 1229\, 1232)\,\
          241\,\
          if(\
            between(n\, 1233\, 1235)\,\
            243\,\
            if(\
              between(n\, 1236\, 1237)\,\
              240\,\
              if(\
                between(n\, 1238\, 1238)\,\
                238\,\
                if(\
                  between(n\, 1239\, 1239)\,\
                  239\,\
                  if(\
                    between(n\, 1240\, 1241)\,\
                    233\,\
                    if(\
                      between(n\, 1242\, 1243)\,\
                      230\,\
                      if(\
                        gte(n\, 1244)\,\
                        229\
                      )\
                    )\
                  )\
                )\
              )\
            )\
          )\
        )\
        :enable=between(t\,49.16\,74.64) [ov];\

        [2:0] \
          scale=823:823 [ov2]; \

        [ov][ov2]\
          overlay=550:24:enable=between(t\,156.96\,162)" -vcodec libx264 -preset ultrafast $RESULT_VIDEO
EOT;
    $this->executeCommand($cmd);
    unlink($imgVideo);
    return $RESULT_VIDEO;
  }
}
