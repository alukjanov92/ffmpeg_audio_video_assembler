<?php

class LogMessage {

  public function __construct () {
    $this->headers = "";
    $this->body = "";
    $this->logFilePath = "/tmp/operations.log";
    $this->shouldFlush = false;
  }

  public function setHeaders ($requestParams) {
    $gender = $requestParams->getGender();
    $name = $requestParams->getName();
    $videoType = $requestParams->getVideoType();
    $videoSubType = isset($_REQUEST["photo"]) ? "image" : "text";

    $str = <<<EOT


==============================================================
  gender - $gender
  name   - $name
  video_type - $videoType
  subtype - $videoSubType
==============================================================
EOT;

    $this->headers = $str;
  }

  public function addCommand ($cmd, $output, $exitCode) {
    $str = <<<EOT

--------------------------------------------------------------
  $cmd
--------------------------------------------------------------
  Exit Code - $exitCode
--------------------------------------------------------------
  $output
--------------------------------------------------------------

EOT;

    $this->body .= $str;
    if ($exitCode !== 0) {
      $this->shouldFlush = true;
    }
  }


  public function flushMessage () {
    if ($this->shouldFlush !== true) {
      return false;
    }

    $logMessage = <<<EOT
$this->headers . $this->body
====================================================================================================================================


EOT;
    file_put_contents($this->logFilePath, $logMessage, FILE_APPEND);
  }
}
