<?php

include_once "BaseVideoCommand.php";
include_once "Videoparams/ClownParams.php";

class ClownTextVideoProcessor extends ClownVideoProcessor {

  public function __construct ($requestParams) {
    parent::__construct($requestParams);
    $commandParams = new ClownParams();
    $this->commandParams = [
      "audio_params" => $commandParams->getAudioParams($this->requestParams),
      "video_params" => $commandParams->getVideoParams(),
    ];
    $this->translitMap = include "utils/translitMap.php";;
  }

  public function processTextVideo () {
    $audioChannel = $this->processAudio();
    $videoChannel = $this->applyImageToVideoChannel();
    $resultVideo = $this->applyAudioChannelToVideo($videoChannel, $audioChannel);
    unlink($audioChannel);
    unlink($videoChannel);
    return $resultVideo;
  }

  public function processTextImage () {
    $FONT_FILE = "/home/developer/ffmpeg_audio_video_assembler/php/commands/clown.ttf";
    $NAME = iconv('UTF-8', 'UTF-8//IGNORE', strtr($this->requestParams->getName(), $this->translitMap));
    $TEXT = "С днём рождения, \n$NAME";
    $RESULT_FILE = TMP . generateRandomName() . ".png";
    $color = $this->requestParams->getGender() === "boy" ? "#526bdd" : "#cd69e6";
    $cmd = "convert -background 'rgba(0, 0, 0, 0)' -gravity center -fill '$color' -font $FONT_FILE -pointsize 52 label:'$TEXT' -extent 555x555 -colorspace YUV $RESULT_FILE";
    $this->executeCommand($cmd);
    return $RESULT_FILE;
  }

  public function applyImageToVideoChannel () {
    $imageWithText = $this->processTextImage();
    $imgVideo = $this->processImageVideo($imageWithText);
    return $this->overlayWithImage($imgVideo, $imageWithText);
  }

  public function processImageVideo ($textImagePath) {
    $photoWidthPerFrames = [
      65, 128, 192, 252, 313, 368, 419, 464, 504, 534, 551, 555, 555, 545, 535, 520, 505, 490, 474, 460, 448, 439, 433, 433, 433
    ];

    $photoAnglePerFrames = [
      180, 189, 198, 209, 216, 229, 243, 250, 260, 270, 280, 290, 300, 309, 317, 325, 333, 339, 345, 350, 354, 357, 359.5, 360
    ];

    $framesDirPath = TMP . generateRandomName() . "/";
    $resultVideo = TMP.generateRandomName() . ".mov";
    $img = $textImagePath;
    mkdir($framesDirPath);

    foreach ($photoWidthPerFrames as $key => $value) {
      $name = "frame_$key.png";
      $path = $framesDirPath . $name;
      $degree = $photoAnglePerFrames[$key];
      $cmd = "convert $img -background 'rgba(0,0,0,0)' -gravity center -resize $value"."x"."$value^ -gravity center -rotate $degree -extent 770x770 $path";
      $this->executeCommand($cmd);
    }

    $cmd = "ffmpeg -y -i $framesDirPath/frame_%d.png -vcodec copy -framerate 25 $resultVideo";
    $this->executeCommand($cmd);

    return $resultVideo;
  }

  public function overlayWithImage ($imgVideo) {
    $RESULT_VIDEO = TMP . generateRandomName() . ".mp4";
    $SAMPLE = $this->commandParams["video_params"]["sample_path"];
    $cmd = <<<EOT
    ffmpeg -y -i $SAMPLE -itsoffset 00:00:49.16 -i $imgVideo -filter_complex "format=yuv420p, \
      overlay=\
        eval='frame'\
        :x=if(\
          between(\
            n\, 1229\, 1232\
          )\,\
          1018\,\
          if(\
           between(\
            n\, 1233\, 1234\
           )\,\
           1012\,\
           if(\
            between(\
              n\, 1235\, 1235\
            )\,\
            1010\,\
            if(\
              between(\
                n\, 1236\, 1237\
              )\,\
              1008\,\
              if(\
                between(\
                  n\, 1238\, 1238\
                )\,\
                1007\,\
                if(\
                  between(\
                    n\, 1239\, 1239\
                  )\,\
                  1005\,\
                  if(\
                    between(\
                      n\, 1240\, 1241\
                    )\,\
                    1006\,\
                    if(\
                      between(\
                        n\, 1242\, 1243\
                      )\,\
                      1008\,\
                      if(\
                        between(\
                          n\, 1244\, 1245\
                        )\,\
                        1013\,\
                        if(\
                          between(\
                            n\, 1246\, 1247\
                          )\,\
                          1015\,\
                          if(\
                            between(\
                              n\, 1248\, 1248\
                            )\,\
                            1017\,\
                            1018\
                          )\
                        )\
                      )\
                    )\
                  )\
                )\
              )\
            )\
           )\
          )\
        )\
        :y=if(\
          between(n\, 1229\, 1232)\,\
          241\,\
          if(\
            between(n\, 1233\, 1235)\,\
            243\,\
            if(\
              between(n\, 1236\, 1237)\,\
              240\,\
              if(\
                between(n\, 1238\, 1238)\,\
                238\,\
                if(\
                  between(n\, 1239\, 1239)\,\
                  239\,\
                  if(\
                    between(n\, 1240\, 1241)\,\
                    233\,\
                    if(\
                      between(n\, 1242\, 1243)\,\
                      230\,\
                      if(\
                        gte(n\, 1244)\,\
                        229\
                      )\
                    )\
                  )\
                )\
              )\
            )\
          )\
        )\
        :enable=between(t\,49.16\,74.64)" -vcodec libx264 -preset ultrafast $RESULT_VIDEO
EOT;
    $this->executeCommand($cmd);
    return $RESULT_VIDEO;
  }
}
