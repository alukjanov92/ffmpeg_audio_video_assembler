<?php

include_once "BaseVideoCommand.php";
include_once "Videoparams/DedMorozParams.php";

class DedMorozVideoProcessor extends BaseVideoCommand {

  public function __construct($requestParams) {
    parent::__construct($requestParams);
    $commandParams = new DedMorozParams();
    $this->commandParams = [
      "audio_params" => $commandParams->getAudioParams($this->requestParams),
      "video_params" => $commandParams->getVideoParams(),
      "photo_params" => $commandParams->getPhotoParams()
    ];
  }

  public function processVideo () {
    $audioChannel = $this->processAudio();
    $videoChannel = $this->applyImageToVideoChannel();
    $resultVideo = $this->applyAudioChannelToVideo($videoChannel, $audioChannel);
    unlink($audioChannel);
    unlink($videoChannel);
    return $resultVideo;
  }

  private function processAudio () {
    $audioParams = $this->commandParams["audio_params"];
    $OUTPUT_FILE_PATH = TMP . generateRandomName() . ".mp3";
    $cmd = "ffmpeg -y -i \"concat:" . $audioParams['full_name_audio_channel'] . "\" -c copy $OUTPUT_FILE_PATH";
    $this->executeCommand($cmd);

    $MIXED_WITH_MUSIC = TMP . generateRandomName() . ".mp3";
    $cmd = "ffmpeg -y -i $OUTPUT_FILE_PATH -i " . $audioParams['music'] . " -filter_complex amix=inputs=2:duration=first $MIXED_WITH_MUSIC";
    $this->executeCommand($cmd);
    unlink($OUTPUT_FILE_PATH);
    return $MIXED_WITH_MUSIC;
  }

  private function applyAudioChannelToVideo ($videoPath, $audioPath) {
    $resultVideoPath = TMP . generateRandomName() . ".mp4";
    $cmd = "ffmpeg -y -i $videoPath -i $audioPath -map 0 -map 1 -acodec aac -strict -2 -vcodec copy -shortest $resultVideoPath";
    $this->executeCommand($cmd);
    return $resultVideoPath;
  }

  private function applyImageToVideoChannel () {
    $img = $this->requestParams->getPhoto();
    $videoParams = $this->commandParams["video_params"];
    $imageParams = $this->commandParams["photo_params"];

    $INPUT_VIDEO = $videoParams["sample_path"];
    $OVERLAY_X_SHIFT = $imageParams["x"];
    $OVERLAY_Y_SHIFT = $imageParams["y"];
    $OVERLAY_START_FRAME = $imageParams["start_frame"];
    $OVERLAY_START_FADE_FRAMES = $imageParams["start_fade_frames"];
    $OVERLAY_END_FRAME = $imageParams["end_frame"];
    $OVERLAY_END_FADE_FRAMES = $imageParams["end_fade_frames"];
    $START_SECONDS = $imageParams["start_second"];
    $END_SECONDS = $imageParams["end_second"];
    $OUTPUT_FILE_NAME = TMP . generateRandomName() . ".mp4";
    $cmd = "ffmpeg -y -i $INPUT_VIDEO -f image2 -loop 1 -r 24 -i $img -preset ultrafast -filter_complex \"[1:0] format=pix_fmts=yuva420p, fade=in:$OVERLAY_START_FRAME:$OVERLAY_START_FADE_FRAMES:alpha=1, fade=out:$OVERLAY_END_FRAME:$OVERLAY_END_FADE_FRAMES:alpha=1 [ov]; [0:v][ov] overlay=$OVERLAY_X_SHIFT:$OVERLAY_Y_SHIFT\" -t 00:03:23 $OUTPUT_FILE_NAME";
    $this->executeCommand($cmd);
    unlink($img);
    return $OUTPUT_FILE_NAME;
  }
}
