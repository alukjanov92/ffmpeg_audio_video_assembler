<?php

include "LogMessage.php";

class RequestMOck {
  public function getVideoType () {
    return $this->getRequestParam("video_type");
  }

  public function getName () {
    return $this->getRequestParam("name");
  }

  public function getPhoto () {
    $fullPath = TMP . generateRandomName() . pathinfo($_FILES["photo"]["name"], PATHINFO_EXTENSION);
    move_uploaded_file($_FILES["photo"]["tmp_name"], $fullPath);
    return $fullPath;
  }

  public function getGender () {
    return $this->getRequestParam("gender");
  }

  public function getToken () {
    return $this->getRequestParam("token");
  }

  public function getRequestParam ($name) {
    return $name;
  }

  public function extractTextParams () {
    $data = [
      "video_type" => $this->getVideoType(),
      "name" => $this->getName(),
      "gender" => $this->getGender(),
      "token" => $this->getToken()
    ];

    return $data;
  }

  public function extractImageParams () {
    $imageParams = $this->extractTextParams();
    $imageParams["photo"] = $this->getPhoto();

    return $imageParams;
  }

}


$logMessage = new LogMessage();
$logMessage->setHeaders(new RequestMOck());
 $cmd = "ffmpeg -y -i 'concat:/home/developer/ffmpeg_audio_video_assembler/media_src/clown/audio/first_part.mp3|/home/developer/ffmpeg_audio_video_assembler/media_src/clown/audio/name_samples/boy/Aleksandr.mp3|/home/developer/ffmpeg_audio_video_assembler/media_src/clown/audio/second_part.mp3' -c copy /home/developer/ffmpeg_audio_video_assembler/php/videoprocessor/queue/../../../tmp/test1.mp3 2>&1";
$output = system($cmd, $retCode);
$logMessage->addCommand($cmd, $output, $retCode);
$logMessage->flushMessage();
