<?php

include_once "utils/fontShiftCalculator.php";
include_once "utils/uppercase.php";

class TransformerTextVideoProcessor extends TransformerVideoProcessor {

  public function __construct ($requestParams) {
    BaseVideoCommand::__construct($requestParams);
    $commandParams = new TransformerParams();
    $this->commandParams = [
      "audio_params" => $commandParams->getAudioParams($this->requestParams),
      "video_params" => $commandParams->getVideoParams()
    ];
    $this->translitMap = include "utils/translitMap.php";
  }

  public function processTextVideo () {
    $audioChannel = $this->processAudio();
    $videoChannel = $this->applyTextToVideoChannel();
    $resultVideo = $this->applyAudioChannelToVideo($videoChannel, $audioChannel);
    unlink($audioChannel);
    unlink($videoChannel);
    return $resultVideo;
  }

  public function applyTextToVideoChannel () {
    $videoParams = $this->commandParams["video_params"];
    $INPUT_VIDEO = $videoParams["sample_path"];
    $OUTPUT_FILE_NAME = TMP . generateRandomName() . ".mp4";
    $translittedName = iconv('UTF-8', 'UTF-8//IGNORE', strtr($this->requestParams->getName(), $this->translitMap));
    $NAME = ru_uppercase($translittedName . "!"); //to count wo slash, see :36
    $TEXT = ru_uppercase("С днем рождения,");
    $PHRASE_X_SHIFT = 560;
    $X_NAME_SHIFT = $PHRASE_X_SHIFT + calculateXShift($NAME, $TEXT, 64, "/home/developer/ffmpeg_audio_video_assembler/php/commands/transformers.ttf");
    $NAME = ru_uppercase($translittedName . "\!");
    $color = "8fc4e4";

    $cmd = <<<EOT
ffmpeg -i $INPUT_VIDEO -preset ultrafast -filter_complex "
  drawtext=text='$TEXT':fontfile=/home/developer/ffmpeg_audio_video_assembler/php/commands/transformers.ttf:fontsize=64:x=$PHRASE_X_SHIFT:y=465:fontcolor_expr=$color%{eif\\\\\\\\: max(0\\\\, min(255\\\\, 255*(1*between(t\\\\, 40.840 + 0.5\\\\, 45.260 - 0.5) + ((t - 40.840)/0.5)*between(t\\\\, 40.840\\\\, 40.840 + 0.5) + (-(t - 45.260)/0.5)*between(t\\\\, 45.260 - 0.5\\\\, 45.260) ) )) \\\\\\\\: x\\\\\\\\: 2 @},
  drawtext=text='$NAME':fontfile=/home/developer/ffmpeg_audio_video_assembler/php/commands/transformers.ttf:fontsize=64:x=$X_NAME_SHIFT:y=565:fontcolor_expr=$color%{eif\\\\\\\\: max(0\\\\, min(255\\\\, 255*(1*between(t\\\\, 40.840 + 0.5\\\\, 45.260 - 0.5) + ((t - 40.840)/0.5)*between(t\\\\, 40.840\\\\, 40.840 + 0.5) + (-(t - 45.260)/0.5)*between(t\\\\, 45.260 - 0.5\\\\, 45.260) ) )) \\\\\\\\: x\\\\\\\\: 2 @}
" -y -acodec copy $OUTPUT_FILE_NAME
EOT;
    $this->executeCommand($cmd);
    return $OUTPUT_FILE_NAME;
  }

}
