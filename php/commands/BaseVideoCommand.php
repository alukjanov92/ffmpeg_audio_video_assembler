<?php

include_once "RequestParams.php";
include_once "LogMessage.php";

class BaseVideoCommand {
  public function __construct ($requestParams) {
    $this->requestParams = $requestParams;
    $this->logMessage = new LogMessage();
    $this->logMessage->setHeaders($requestParams);
  }

  public function logCmd ($cmd, $output, $exitCode) {
    $this->logMessage->addCommand($cmd, $output, $exitCode);
  }

  public function executeCommand ($cmd, $debug = false) {
    $retCode = 0;
    $cmdWithErrorRedirect = $cmd . " 2>&1";
    $output = system($cmdWithErrorRedirect, $retCode);
    if ($debug === true) {
      var_dump($cmd);
      die;
    }
    if ($retCode !== 0) {
      throw new Exception($output);
    }
    $this->logCmd($cmd, $output, $retCode);
  }


  public function __destruct () {
    $this->logMessage->flushMessage();
  }
}
