<?php

include_once "utils/fontShiftCalculator.php";
include_once "utils/uppercase.php";

class FeyaTextVideoProcessor extends FeyaVideoProcessor {
  public function __construct ($requestParams) {
    $commandParams = new FeyaParams();
    BaseVideoCommand::__construct($requestParams);
    $this->commandParams = [
      "audio_params" => $commandParams->getAudioParams($this->requestParams),
      "video_params" => $commandParams->getVideoParams()
    ];
    $this->translitMap = include "utils/translitMap.php";
  }

  public function processTextVideo () {
    $audioChannel = $this->processAudio();
    $videoChannel = $this->applyTextToVideoChannel();
    $resultVideo = $this->applyAudioChannelToVideo($videoChannel, $audioChannel);
    unlink($audioChannel);
    unlink($videoChannel);
    return $resultVideo;
  }

  public function applyTextToVideoChannel () {
    $videoParams = $this->commandParams["video_params"];

    $INPUT_VIDEO = $videoParams["sample_path"];
    $OUTPUT_FILE_NAME = TMP . generateRandomName() . ".mp4";
    $X_SHIFT = 700;
    $TEXT = "С днем рождения,";
    $NAME =  iconv('UTF-8', 'UTF-8//IGNORE', strtr($this->requestParams->getName(), $this->translitMap));
    $NAME_X_SHIFT = $X_SHIFT + calculateXShift($NAME, $TEXT, 72, "/home/developer/ffmpeg_audio_video_assembler/feya.ttf");
    $cmd = <<<EOT
ffmpeg -i $INPUT_VIDEO -preset ultrafast -filter_complex "
  drawtext=text='$TEXT':fontfile=/home/developer/ffmpeg_audio_video_assembler/feya.ttf:fontsize=72:x=$X_SHIFT:y=500:fontcolor_expr=ed09c3%{eif\\\\\\\\: max(0\\\\, min(255\\\\, 255*(1*between(t\\\\, 47.691 + 0.5\\\\, 52.119 - 0.5) + ((t - 47.691)/0.5)*between(t\\\\, 47.691\\\\, 47.691 + 0.5) + (-(t - 52.119)/0.5)*between(t\\\\, 52.119 - 0.5\\\\, 52.119) ) )) \\\\\\\\: x\\\\\\\\: 2 @},
  drawtext=text='$NAME':fontfile=/home/developer/ffmpeg_audio_video_assembler/feya.ttf:fontsize=72:x=$NAME_X_SHIFT:y=590:fontcolor_expr=ed09c3%{eif\\\\\\\\: max(0\\\\, min(255\\\\, 255*(1*between(t\\\\, 47.691 + 0.5\\\\, 52.119 - 0.5) + ((t - 47.691)/0.5)*between(t\\\\, 47.691\\\\, 47.691 + 0.5) + (-(t - 52.119)/0.5)*between(t\\\\, 52.119 - 0.5\\\\, 52.119) ) )) \\\\\\\\: x\\\\\\\\: 2 @}
" -t 00:01:56 -y -acodec copy $OUTPUT_FILE_NAME
EOT;

      $this->executeCommand($cmd);
      return $OUTPUT_FILE_NAME;
  }

}
