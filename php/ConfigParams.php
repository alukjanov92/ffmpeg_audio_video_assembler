<?php

class ConfigParams {

  public function __construct() {
    $serviceConfig = include "config/service.php";
    $this->ftp_config = $serviceConfig['ftp_storage'];
    $this->site_config = $serviceConfig['site'];
  }

  public function getSiteApiParams(){
    return $this->site_config;
  }

  public function getFtpParams(){
    return $this->ftp_config;
  }

}
