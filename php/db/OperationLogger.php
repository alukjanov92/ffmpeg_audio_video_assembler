<?php
  include "exceptions/DbException.php";
  class OperationLogger{
    public function __construct(){
      $this->params = include "config/dbConfig.php";
      $ERROR_CODES = include "config/errorCodes.php";
      $this->errorCodes = $ERROR_CODES["db"];
      $db = $this->params['host'];
      $user = $this->params['user'];
      $pswd = $this->params['password'];
      $db_name = $this->params['db_name'];
      if(($this->dbResource = mysqli_connect($db, $user, $pswd, $db_name)) === false){
        throw new DbException($this->errorCodes["21"], 21);
      }
    }

    public function isTokenUnique($token){
      return $this->getSingleField($this->params['tbl_name'], 'id', 'token="' . $token . '"') === null;
    }

    public function registerOperation($token){
      $status = "registered";
      $query = "INSERT INTO " . $this->params['tbl_name'] . "(token, status, register_time) VALUES ('" . $token . "','" . $status . "', now())";
      $this->execQuery($query);
    }

    public function updateOperationProcessing($token) {
      $status = "processing";
      $tableName = $this->params["tbl_name"];
      $query = <<<EOT
        UPDATE $tableName SET
          start_time = now(),
          status = '$status'
        WHERE
          token = '$token'
EOT;
      $this->execQuery($query);
    }

    public function updateOperationSuccess($token, ResultFile $file){
      $status = "success";
      $fileName = $file->getFileName();
      $this->updateOperationWithFinalStatus($token, $status, null, null, $fileName);
    }

    public function updateOperationFail($token, $info, $code){
      $status = "failed";
      $this->updateOperationWithFinalStatus($token, $status, $info, $code);
    }

    public function getOperationStatus($token){
      $res = $this->execQuery("
        SELECT status, info, code FROM " . $this->params['tbl_name'] .
        " WHERE token='" . $token . "'");
      $data = mysqli_fetch_assoc($res);
      return is_null($data) === false ? $data : null;
    }

    private function updateOperationWithFinalStatus($token, $status, $info, $code, $fileName = ""){
      $query = "UPDATE " . $this->params['tbl_name'] . " SET " .
        "finish_time = now(), " .
        "status = '" . $status . "'
        " . ($code ? ", code = " . $code : "") . "
        " . ($info ? ", info = \"" . $info . "\"" : "") . "
        " . ($fileName ? ", file = \"" . $fileName . "\"" : "") . "
        WHERE token='" . $token . "'";

      $this->execQuery($query);
    }

    private function execQuery($query){
      $res = mysqli_query($this->dbResource, $query);
      if(($error = mysqli_error($this->dbResource)) !== ""){
        throw new DbException(mysqli_error($this->dbResource), 22);
      }
      return $res;
    }

    public function getSingleField($tblName, $field, $condition){
      $query = "
        SELECT $field FROM $tblName
        WHERE $condition
      ";
      $res = $this->execQuery($query);
      $ret = mysqli_fetch_assoc($res);
      return is_null($ret) ? null : $ret[$field];
    }
  }
?>
