<?php
  class SiteApiHelper {
    public function __construct($params){
      $this->params = $params->getSiteApiParams();
    }

    public function acceptRequest($responseBody){
      if(DEBUG === true)
        return true;
      $code = 202;
      http_response_code($code);
      echo json_encode($responseBody);
      fastcgi_finish_request();
    }

    public function declineRequest($msg, $errorCode){
      $code = 400;
      http_response_code($code);
      echo json_encode([
        'error' => $errorCode,
        'info' => $msg
      ]);
    }

    static function processFileDownload($filePath){
      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename='.basename($filePath));
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      header('Content-Length: ' . filesize($filePath));
      ob_get_clean();
      readfile($filePath);
      ob_end_flush();
      unlink($filePath);
    }

    public function sendOperationSucces($token, ResultFile $file){
      $linktoFile = $file->fileLink;
      $fileSize = $file->fileSize;
      $operationCodes = $this->params["operation_status_codes"];

      $result = [
        'token'=> $token,
        'status'=> $operationCodes["OPERATION_SUCCESS"],
        'file'=> $file->fileLink,
        'size'=> $file->fileSize,
        'sha'=> $file->fileSHA256,
      ];
      $this->sendRequestToSite($result);
    }

    public function sendOperationFail($token, $message, $errorCode){
      $opCodes = $this->params["operation_status_codes"];
      $result = [
        'token'=> $token,
        'status'=> $opCodes["OPERATION_FAILED"],
        'error'=> $errorCode,
        'info'=> $message
      ];
      $this->sendRequestToSite($result);
    }

    public function sendOperationCurrentStatus($token, $status, $errorCode = null, $message = null){
      $opCodes = $this->params["operation_status_codes"];
      $result = [
        'token'=> $token,
        'status'=> $status,
        'error'=> $errorCode,
        'info'=> $message
      ];
      echo json_encode($result);
    }

    private function sendRequestToSite($data){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $this->params['address']);
      curl_setopt($ch, CURLOPT_POST,1);
      curl_setopt($ch, CURLOPT_VERBOSE,0);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $result=curl_exec($ch);
      curl_close ($ch);
    }

    public function sendNamesList($namesList){
      http_response_code(200);
      echo json_encode($namesList, JSON_UNESCAPED_UNICODE);
    }

    public function sendVideoTypes ($videoTypes) {
      http_response_code(200);
      echo json_encode($videoTypes);
    }
  }
?>
