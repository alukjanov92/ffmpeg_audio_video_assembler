<?php
include_once "Videoparams/DedMorozParams.php";
include_once "Videoparams/FeyaParams.php";
include_once "Videoparams/TransformerParams.php";
include_once "Videoparams/ClownParams.php";

function getBaseName ($path, $suffix) {
  $parts = explode("/", $path);
  $last = array_pop($parts);
  return str_replace($suffix, "", $last);
}

  class NamesList{

    public function __construct(){
      $this->dedMorozParams = new DedMorozParams();
      $this->feyaParams = new FeyaParams();
      $this->transformerParams = new TransformerParams();
      $this->clownParams = new ClownParams();
      $this->translitMap = include "utils/translitMap.php";
    }

    public function getSupportedNamesList($video_type){
      if ($video_type === "dedmoroz") {
        $boysNamesSamples = $this->dedMorozParams->getBoysNames();
        $girlsNamesSamples = $this->dedMorozParams->getGirlsNames();

        return [
          "boy" => $this->getAssocList($this->getDirectoryListing($boysNamesSamples)),
          "girl" => $this->getAssocList($this->getDirectoryListing($girlsNamesSamples)),
        ];
      } else if ($video_type === "feya") {
        $namesSamplesPath = $this->feyaParams->getNamesPath();
        return [
          "girl" => $this->getAssocList($this->getDirectoryListing($namesSamplesPath))
        ];
      } else if ($video_type === "transformer") {
        $namesSamplesPath = $this->transformerParams->getNamesPath();
        return [
          "boy" => $this->getAssocList($this->getDirectoryListing($namesSamplesPath))
        ];
      } else if ($video_type === "clown") {
        $boysNamesPath = $this->clownParams->getBoysNames();
        $girlsNamesPath = $this->clownParams->getGirlsNames();

        return [
          "girl" => $this->getAssocList($this->getDirectoryListing($girlsNamesPath)),
          "boy" => $this->getAssocList($this->getDirectoryListing($boysNamesPath))
        ];
      }
    }

    public function getDirectoryListing($filePath){
      $ret = glob($filePath . "*");
      foreach($ret as $key => $value){
        $ret[$key] = getBaseName($value, '.mp3');
      }
      return $ret;
    }

    public function getTransliterratedList($dirListing){
      $ret = [];
      foreach($dirListing as $key => $value){
        $translitted = iconv('UTF-8', 'UTF-8//IGNORE', strtr($value, $this->translitMap));
        $ret[$value] = $translitted;
      }
      return $ret;
    }

    public function getAssocList ($arr) {
      $ret = [];
      foreach($arr as $key => $value){
        $ret[$value] = $value;
      }

      return $ret;
    }
  }
?>
