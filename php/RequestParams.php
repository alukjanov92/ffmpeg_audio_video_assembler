<?php

class RequestParams {

  public function getVideoType () {
    return $this->getRequestParam("video_type");
  }

  public function getName ($escape = false) {
    $name = $this->getRequestParam("name");
    if ($escape === true) {
      $replace = [
        "`"
      ];

      $to_replace = array_map(function ($item) {
        return "\\".$item;
      }, $replace);

      $name = str_replace($replace, $to_replace, $name);
    }

    return $name;
  }

  public function getPhoto () {
    if (!$this->photoPath) {
      $this->photoPath = TMP . generateRandomName() . pathinfo($_FILES["photo"]["name"], PATHINFO_EXTENSION);
      move_uploaded_file($_FILES["photo"]["tmp_name"], $this->photoPath);
    }

    return $this->photoPath;
  }

  public function getGender () {
    return $this->getRequestParam("gender");
  }

  public function getToken () {
    return $this->getRequestParam("token");
  }

  public function getRequestParam ($name) {
    return isset($_REQUEST[$name]) ? $_REQUEST[$name] : null;
  }

  public function extractTextParams () {
    $data = [
      "video_type" => $this->getVideoType(),
      "name" => $this->getName(),
      "gender" => $this->getGender(),
      "token" => $this->getToken()
    ];

    return $data;
  }

  public function extractImageParams () {
    $imageParams = $this->extractTextParams();
    $imageParams["photo"] = $this->getPhoto();

    return $imageParams;
  }

}
