<?php

  define("DESTINATION_PATH", "/");

  include "exceptions/FtpException.php";


  Class FtpManager{
    public function __construct($params){
      $this->params = $params->getFtpParams();
      $this->ftp = $this->initFtpConnector();
      $ERROR_CODES = include "config/errorCodes.php";
      $this->errorCodes = $ERROR_CODES["ftp"];
    }

    private function initFtpConnector(){
      if(($sftp = ftp_connect($this->params["host"])) !== false){
        if(ftp_login($sftp, $this->params["user"], $this->params["password"]) !== false){
          if(ftp_pasv($sftp, true)){
            return $sftp;
          }
        }
      }
      throw new FtpException($this->errorCodes["11"], 11);
    }

    public function uploadVideoToFtp(&$resultFile){
      $videoFilePath = $resultFile->filePath;
      $videoFileName = $resultFile->getFileName();
      $destination = DESTINATION_PATH . $videoFileName;
      $flist = ftp_nlist($this->ftp, ".");
      if(in_array($videoFileName, $flist)){
        throw new FtpException($this->errorCodes["13"], 13);
      }
      if(!ftp_put($this->ftp, $destination, $videoFilePath, FTP_BINARY)){
        $lastError = error_get_last();
        throw new FtpException($lastError["message"], 12);
      }
      
      $linkToFile = $this->params['host'] . $destination;
      $resultFile->setFileLink($destination);
    }

    public function __destruct(){
      ftp_close($this->ftp);
    }
  }
?>
