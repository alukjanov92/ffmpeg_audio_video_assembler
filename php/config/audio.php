<?php
  $AUDIO_PARAMS = [
    "path_to_audio_channels" => ROOT_PATH . "media_src/audio/",
    "audio_channels" => [
      "dedmoroz" => [
        "parts_to_join" => [
          "first_part.mp3", "second_part.mp3", "third_part.mp3"
        ]
      ]
    ]
  ];
  return $AUDIO_PARAMS;
?>
