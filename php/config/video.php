<?php
  $VIDEO_PARAMS = [
    "path_to_video_channels" => ROOT_PATH . "media_src/video/sample/",
    "video_channels" => [
      "dedmoroz" => [
        "sample_path" => "congratulations_video.mp4",
        "x_photo_shift" => "300",
        "y_photo_shift" => "300",
        "start_seconds" => "1",
        "end_seconds" => "5",
        "crf" => "15"
      ]
    ]
  ];
  return $VIDEO_PARAMS;
?>
