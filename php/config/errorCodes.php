<?php
  return [
    "params" => [
      "1" => "PHOTO_IS_NOT_PROCESSABLE",
      "2" => "UNSUPPORTED_VIDEO_TYPE",
      "3" => "TOKEN_IS_EMPTY",
      "4" => "TOKEN_IS_ALREADY_REGISTERED",
      "5" => "NAME_SAMPLE_DOES_NOT_EXISTS",
      "6" => "NAME_IS_INCORRECT"
    ],

    "ftp" => [
      "11" => "FTP_SERVER_IS_NOT_ACCESSIBLE",
      "12" => "COULD_NOT_WRITE_FILE_TO_FTP",
      "13" => "FILE_ALREADY_EXISTS_ON_SERVER"
    ],

    "db" => [
      "21" => "COULD_NOT_CONNECT_TO_DATABASE",
      "22" => "MYSQL_EXEC_QUERY_ERROR",
      "23" => "ENTITY_NOT_FOUND"
    ]
  ];
?>
