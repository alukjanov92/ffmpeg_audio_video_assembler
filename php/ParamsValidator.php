<?php

include "exceptions/ParamsException.php";

include "Videoparams/DedMorozParams.php";
include "Videoparams/FeyaParams.php";
include "Videoparams/TransformerParams.php";
include "Videoparams/ClownParams.php";

function nameChecker ($requestParams) {
  $params;

  $videoType = $requestParams->getVideoType();

  switch ($videoType) {
    case "dedmoroz"     : $params = new DedMorozParams();
                          break;
    case "transformer"  : $params = new TransformerParams();
                          break;
    case "feya"         : $params = new FeyaParams();
                          break;
    case "clown"        : $params = new ClownParams();
                          break;
  }


  $fileName = $params->getNameSample($requestParams, "first/");
  $fileName = str_replace(
    [
      "\\"
    ],
    [
      ""
    ],
    $fileName
  );

  return file_exists($fileName);

}

class ParamsValidator {

  public function __construct () {
    $ERROR_CODES = include "config/errorCodes.php";
    $this->errorCodes = $ERROR_CODES["params"];
  }

  private function checkToken ($requestParams, $tokenUniqueChecker = null) {
    $token = $requestParams->getToken();
    if (is_null($token)) {
      throw new ParamsException($this->errorCodes["3"], 3);
    }

    if ($tokenUniqueChecker && $tokenUniqueChecker->isTokenUnique($token) === false) {
      throw new ParamsException($this->errorCodes["4"], 4);
    }
  }

  private function checkPhoto ($requestParams) {
    if ($_FILES["photo"]["error"] !== 0) {
      throw new ParamsException($this->errorCodes["1"], 1);
    }
  }

  public function checkVideoType ($requestParams) {
    $existentVideoTypes = ["dedmoroz", "feya", "transformer", "clown"];
    $requestedVideoType = $requestParams->getVideoType();
    if (in_array($requestedVideoType, $existentVideoTypes) === false) {
      throw new ParamsException($this->errorCodes["2"], 2);
    }
  }

  public function validateVideoRequest ($requestParams, $tokenUniqueChecker = null) {
    $this->checkToken($requestParams, $tokenUniqueChecker);
    $this->checkPhoto($requestParams);
    $this->checkVideoType($requestParams);
  }

  public function validateNamesRequest ($requestParams) {
    $this->checkVideoType($requestParams);
  }

  public function checkName($requestParams) {
    $name = $requestParams->getName();
    if (!$name) {
      throw new ParamsException($this->errorCodes["6"], 6);
    }

    if (nameChecker($requestParams) !== true) {
      throw new ParamsException($this->errorCodes["5"], 5);
    }
  }

  public function checkTextVideoType ($requestParams) {
    $existentVideoTypes = ["feya", "transformer", "clown"];
    $requestedVideoType = $requestParams->getVideoType();
    if (in_array($requestedVideoType, $existentVideoTypes) === false) {
      throw new ParamsException($this->errorCodes["2"], 2);
    }
  }

  public function validateTextVideoRequest ($requestParams, $tokenUniqueChecker) {
    $this->checkToken($requestParams, $tokenUniqueChecker);
    $this->checkName($requestParams);
    $this->checkTextVideoType($requestParams);
  }

}
