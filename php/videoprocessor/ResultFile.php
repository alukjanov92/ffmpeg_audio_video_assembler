<?php
  Class ResultFile{
    public function __construct(){
      $this->filePath = "";
      $this->fileLink = null;
      $this->fileSize = 0;
      $this->fileSHA256 = null;
    }

    public function setFilePath($value){
      $this->filePath = $value;
      $this->fileSize = filesize($this->filePath);
      $this->fileSHA256 = $this->calculateFileSumSHA256($this->filePath);
    }

    public function setFileLink($value){
      $this->fileLink = $value;
    }

    public function getFileName(){
      $fp = $this->filePath;
      return basename($fp);
    }

    public function renameFile ($requestParams, $videoType) {
      $oldName = $this->getFileName();
      $pathToFile = dirname($this->filePath);

      $parts = [
        $requestParams->getVideoType(),
        $videoType,
        $requestParams->getName(),
        $requestParams->getGender(),
        substr($oldName, -10)
      ];

      $newName = $pathToFile . "/" . join($parts, "_");
      rename($this->filePath, $newName);
      $this->setFilePath($newName);
    }

    private function calculateFileSumSHA256($filePath){
      return hash_file("sha256", $filePath);
    }

    public function __destruct () {
      if ($this->filePath) {
        unlink($this->filePath);
      }
    }
  }
?>
