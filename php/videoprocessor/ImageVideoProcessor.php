<?php

  include_once "ResultFile.php";
  include_once "commands/DedMorozVideoProcessor.php";
  include_once "commands/FeyaVideoProcessor.php";
  include_once "commands/TransformerVideoProcessor.php";
  include_once "commands/ClownVideoProcessor.php";

  class ImageVideoProcessor {
    public function processVideo ($requestParams) {
      $videoProcessor = null;
      $videoType = $requestParams->getVideoType();

      switch ($videoType) {
        case "dedmoroz": $videoProcessor = new DedMorozVideoProcessor($requestParams);
        break;

        case "feya": $videoProcessor = new FeyaVideoProcessor($requestParams);
        break;

        case "transformer": $videoProcessor = new TransformerVideoProcessor($requestParams);
        break;

        case "clown": $videoProcessor = new ClownVideoProcessor($requestParams);
        break;

        default: throw new Exception("Wrong video type");

      }

      $resultVideo = $videoProcessor->processVideo();
      $resultFile = new ResultFile();
      $resultFile->setFilePath($resultVideo);

      return $resultFile;
    }

  }
