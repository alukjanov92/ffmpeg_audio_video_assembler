<?php

  include_once "RequestParams.php";

  class VideoRequestParams extends RequestParams {

    private $data = null;

    public function __construct ($data) {
      $this->data = $data;
    }

    public function getRequestParam ($name) {
      return isset($this->data[$name]) ? $this->data[$name] : null;
    }

    public function getPhoto () {
      return $this->getRequestParam("photo");
    }
  }
