<?php
  include "ResultFile.php";
  include "commands/DedMorozVideoProcessor.php";
  include "commands/FeyaVideoProcessor.php";
  include "commands/TransformerVideoProcessor.php";
  include "commands/ClownVideoProcessor.php";

  include "commands/FeyaTextVideoProcessor.php";
  include "commands/TransformerTextVideoProcessor.php";
  include "commands/ClownTextVideoProcessor.php";

  class VideoAssembler {
    public function processVideo ($requestParams) {
      $videoProcessor = null;
      $videoType = $requestParams->getVideoType();

      switch ($videoType) {
        case "dedmoroz": $videoProcessor = new DedMorozVideoProcessor($requestParams);
        break;

        case "feya": $videoProcessor = new FeyaVideoProcessor($requestParams);
        break;

        case "transformer": $videoProcessor = new TransformerVideoProcessor($requestParams);
        break;

      }

      $resultVideo = $videoProcessor->processVideo();
      $resultFile = new ResultFile();
      $resultFile->setFilePath($resultVideo);

      return $resultFile;
    }

    public function processTextVideo ($requestParams) {
      $videoProcessor = null;
      $videoType = $requestParams->getVideoType();

      switch ($videoType) {
        case "feya": $videoProcessor = new FeyaTextVideoProcessor($requestParams);
        break;

        case "transformer": $videoProcessor = new TransformerTextVideoProcessor($requestParams);
        break;

        case "clown": $videoProcessor = new ClownTextVideoProcessor($requestParams);
        break;

        default: break;
      }
      $resultVideo = $videoProcessor->processTextVideo();
      $resultFile = new ResultFile();
      $resultFile->setFilePath($resultVideo);

      return $resultFile;
    }

  }
?>
