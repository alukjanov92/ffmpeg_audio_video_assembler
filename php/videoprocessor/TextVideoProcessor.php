<?php

  include_once "ResultFile.php";
  include_once "commands/DedMorozVideoProcessor.php";
  include_once "commands/FeyaVideoProcessor.php";
  include_once "commands/TransformerVideoProcessor.php";
  include_once "commands/ClownVideoProcessor.php";

  include_once "commands/FeyaTextVideoProcessor.php";
  include_once "commands/TransformerTextVideoProcessor.php";
  include_once "commands/ClownTextVideoProcessor.php";

  class TextVideoProcessor {
    public function processVideo ($requestParams) {
      $videoProcessor = null;
      $videoType = $requestParams->getVideoType();

      switch ($videoType) {
        case "feya": $videoProcessor = new FeyaTextVideoProcessor($requestParams);
        break;

        case "transformer": $videoProcessor = new TransformerTextVideoProcessor($requestParams);
        break;

        case "clown": $videoProcessor = new ClownTextVideoProcessor($requestParams);
        break;

        default: break;
      }

      $resultVideo = $videoProcessor->processTextVideo();
      $resultFile = new ResultFile();
      $resultFile->setFilePath($resultVideo);

      return $resultFile;
    }
  }
