<?php

ini_set("include_path", __DIR__."/../../../php/");
define("ROOT_PATH", __DIR__."/../../../");
define("TMP", ROOT_PATH . "tmp/");
define("DEBUG", false);
define("DEBUG_VIDEO", false);
define("DEBUG_FTP", false);
define("USE_AMAZON", true);
include_once "videoprocessor/TextVideoProcessor.php";
include_once "videoprocessor/ImageVideoProcessor.php";
include_once "AWSUploader/AWSUploader.php";
include_once "siteApi/SiteApiHelper.php";
include_once "db/OperationLogger.php";
include_once "logger.php";
include_once "randomName.php";
include_once "videoprocessor/VideorequestParams.php";
include_once "ConfigParams.php";
include_once "/home/developer/ffmpeg_audio_video_assembler/vendor/autoload.php";

use PhpAmqpLib\Connection\AMQPStreamConnection;

class FFmpegTaskWorker {

  private $connection = null;
  private $channel = null;
  private $logger = null;

  public function __construct () {
    $this->connection = null;
    $this->channel = null;
    $this->logger = new Logger();
    $configParams = new ConfigParams();
    $this->siteApiHelper = new SiteApiHelper($configParams);
  }

  private function compileTextVideo ($requestParams) {
    $processor = new TextVideoProcessor();
    $resultFile = $processor->processVideo($requestParams);

    return $resultFile;
  }

  private function compileImageVideo ($requestParams) {
    $processor = new ImageVideoProcessor();
    $resultFile = $processor->processVideo($requestParams);

    return $resultFile;
  }

  private function getParamsFromData ($data) {
    return new VideoRequestParams($data);
  }

  public function compileVideo ($message) {
    try {
      $data = json_decode($message->body, true);
      $videoData = $data["params"];
      $videoType = $data["videoType"];

      $videorequestParams = $this->getParamsFromData($videoData);
      $operationLogger = new OperationLogger();
      $operationLogger->updateOperationProcessing($videorequestParams->getToken());

      switch ($videoType) {
        case "text": $resultFile = $this->compileTextVideo($videorequestParams);
                      break;

        case "image": $resultFile = $this->compileImageVideo($videorequestParams);
                      break;

        default: throw new Exception("Wrong video type requested (neither image nor text)");
      }

      $this->uploadResultFile($resultFile, $videorequestParams, $videoType);
      $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
    } catch (Exception $e) {
      $token = $videorequestParams->getToken();
      $msg = $e->getMessage();
      $code = $e->getCode();
      $operationLogger = new OperationLogger();
      $operationLogger->updateOperationFail($token, $msg, $code);
      $this->siteApiHelper->sendOperationFail($token, $msg, $code);

      $this->channel->close();
      $this->connection->close();

      $this->registerQueueConsumerCallback();
    }

  }

  public function uploadResultFile ($resultFile, $requestParams, $videoType) {
    $awsUploader = new AWSUploader();
    $resultFile->renameFile($requestParams, $videoType);
    $awsUploader->uploadFileToS3($resultFile);

    $token = $requestParams->getToken();

    $operationLogger = new OperationLogger();
    $operationLogger->updateOperationSuccess($token, $resultFile);
    $this->siteApiHelper->sendOperationSucces($token, $resultFile);
  }

  public function registerQueueConsumerCallback () {
    echo "registered!";
    $this->connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
    $this->channel = $this->connection->channel();

    $this->channel->queue_declare('video_compilation_queue', false, false, false, false);

    $cb = function ($msg) {
      $this->compileVideo($msg);
    };

    $cb = $cb->bindTo($this);

    $this->channel->basic_consume('video_compilation_queue', '', false, false, false, false, $cb);

    while(count($this->channel->callbacks)) {
          $this->channel->wait();
    }


    $this->channel->close();
    $this->connection->close();
  }

}

function spawnWorker() {
  $worker = new FFmpegTaskWorker();
  $worker->registerQueueConsumerCallback();
}

spawnWorker();
