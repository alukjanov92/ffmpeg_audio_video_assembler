<?php

  function concatAssocKeyValue(&$value, $key){
    $value = $key . " " . $value;
  }
  
  class ImageProcessor{
    public function __construct($params){
      $this->imageParams = $params;
    }

    public function rotateImage($imgPath){
      
    }

    public function processPerspective($imgPath){
      $X_SHIFT = $this->imageParams["photo_perspective_x_shift"];
      $Y_SHIFT = $this->imageParams["photo_perspective_y_shift"];
      $START_X = 0;
      $START_Y = 0;
      $W = $this->imageParams["photo_with"];
      $H = $this->imageParams["photo_height"];
      $COORDS = [
        "$START_X,$START_Y" => ($START_X + $X_SHIFT) . ',' . ($START_Y + $Y_SHIFT),
        "$START_X,$H" => ($START_X + $X_SHIFT) . "," . ($H - $Y_SHIFT),
        "$W,0" => "$W,0",
        "$W,$H" => "$W,$H"
      ];
      array_walk($COORDS, "concatAssocKeyValue");
      $resultPath = TMP .generateRandomName() . ".png";
      $cmd = "convert $imgPath -virtual-pixel transparent -distort Perspective '" . implode($COORDS, "  ") . "' $resultPath";
      system($cmd);
      return $resultPath;
    }
  }

?>
