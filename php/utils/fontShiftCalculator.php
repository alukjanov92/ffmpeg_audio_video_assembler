<?php

function pxToPt ($px) {
  return $px * 0.75;
}

function ptToPx ($pt) {
  return $pt * 1.33333;
}

function getTextWidth ($text, $fontSize, $fontfile) {
  $rect = imagettfbbox(pxToPt($fontSize), 0, $fontfile, $text);
  foreach ($rect as $key => $value) {
    $rect[$key] = abs($value);
  }
  return floor($rect[2] - $rect[0]);
}

function calculateXShift ($name, $text, $fontSize, $fontFile) {
  $textWidth = getTextWidth($text, $fontSize, $fontFile);
  $nameWidth = getTextWidth($name, $fontSize, $fontFile);

  $delta = $textWidth - $nameWidth;
  if ($delta < 0) {
    return 0;
  } else {
    return round($delta / 2);
  }
}
