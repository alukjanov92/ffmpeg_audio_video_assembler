<?php

class ClownParams {
  public function __construct() {
    $this->rootVideoPath = "/home/developer/ffmpeg_audio_video_assembler/media_src/clown/";

    $this->videoPath = $this->rootVideoPath . "video/sample/sample.mp4";

    $this->audioPath = $this->rootVideoPath . "audio/";

    $this->nameSamplesPath = $this->audioPath . "name_samples/";

    $this->delimiter = "|";
  }

  public function getBoysNames () {
    return $this->nameSamplesPath . "boy/";
  }

  public function getGirlsNames () {
    return $this->nameSamplesPath . "girl/";
  }

  public function getNameSample ($params) {
    return $this->nameSamplesPath . $params->getGender() . "/" . $params->getName(true) . ".mp3";
  }

  public function getVideoParams () {
    return [
      "sample_path" => $this->videoPath
    ];
  }

  public function getAudioParams ($params) {
    $firstPart = $this->audioPath . "first_part.mp3";
    $secondPart = $this->audioPath . "second_part.mp3";
    return [
      "full_name_audio_channel" => $firstPart . $this->delimiter . $this->getNameSample($params) . $this->delimiter . $secondPart,
      "music" => $this->audioPath . "music.mp3"
    ];
  }
}
