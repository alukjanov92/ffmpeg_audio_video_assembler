<?php

class FeyaParams {
  public function __construct () {
    $this->rootVideoPath = "/home/developer/ffmpeg_audio_video_assembler/media_src/feya/";

    $this->videoPath = $this->rootVideoPath . "video/sample/sample.mp4";

    $this->audioPath = $this->rootVideoPath . "audio/";

    $this->nameSamplesPath = $this->audioPath . "name_samples/";

    $this->delimiter = "|";

    $this->photo_x_shift = 789;
    $this->photo_y_shift = 369;

    $this->photo_start_second = 47.69;
    $this->photo_start_fade_seconds_count = 0.32;

    $this->photo_end_second = 52.1;
    $this->fade_out_start_seconds = 51.85;
    $this->photo_end_fade_seconds_count = 0.3;

    //ffmpeg -y -i Фея_5.mp4 -f image2 -loop 1 -r 24 -i ./don85_392_feya.png -filter_complex "[1:0] format=pix_fmts=yuva420p, fade=t=in:s    t=47.69:d=0.32:alpha=1, fade=t=out:st=51.9:d=0.3:alpha=1 [ov]; [0:v][ov] overlay=775:358:enable='between(t,47.69,52.12)" -preset ul    trafast -shortest -acodec copy test_feya_test_362.mp4


  }

  public function getAudioParams ($requestParams) {
    $firstPart = $this->audioPath . "first_part.mp3";
    $secondPart = $this->audioPath . "second_part.mp3";
    $music = $this->audioPath . "music.mp3";
    return [
      "full_name_audio_channel" => $firstPart . $this->delimiter . $this->getNameSample($requestParams) . $this->delimiter . $secondPart,
      "music" => $music
    ];
  }

  public function getNameSample ($requestParams) {
    return $this->nameSamplesPath . $requestParams->getName(true) . ".mp3";
  }

  public function getNamesPath () {
    return $this->nameSamplesPath;
  }

  public function getPhotoParams () {
    return [
      "x" => $this->photo_x_shift,
      "y" => $this->photo_y_shift,

      "start_second" => $this->photo_start_second,
      "start_fade_seconds" => $this->photo_start_fade_seconds_count,
      "fade_out_start_seconds" => $this->fade_out_start_seconds,
      "fade_out_duration" => $this->photo_end_fade_seconds_count,
      "end_second" => $this->photo_end_second
    ];
  }

  public function getVideoParams () {
    return [
      "sample_path" => $this->videoPath
    ];
  }

}
