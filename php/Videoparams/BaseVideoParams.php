<?php

interface VideoParamsInterface {
  
  public function getAudioConcatString (); 
  public function getPhotoParams ();
}
