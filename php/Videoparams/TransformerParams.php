<?php

class TransformerParams {
  public function __construct () {
    $this->rootVideoPath = "/home/developer/ffmpeg_audio_video_assembler/media_src/transformer/";

    $this->videoPath = $this->rootVideoPath . "video/sample/sample.mp4";

    $this->audioPath = $this->rootVideoPath . "audio/";

    $this->nameSamplesPath = $this->audioPath . "name_samples/";

    $this->delimiter = "|";

    $this->photo_x_shift = 499;
    $this->photo_y_shift = 80;



    $this->photo_start_second = 40.84;
    $this->photo_start_fade_seconds_count = 0.5;
    $this->photo_end_second = 45.26;
    $this->photo_end_fade_seconds_count = 0.36;
    //ffmpeg -y -i Invictus_3.mp4 -f image2 -loop 1 -r 24 -i ./don85_921_invictus.png -filter_complex "[1:0] format=pix_fmts=yuva420p, fad    e=t=in:st=40.64:d=0.5:alpha=1, fade=t=out:st=45:d=0.36:alpha=1 [ov]; [0:v][ov] overlay=499:80:enable='between(t,40.84,45.26)" -prese    t ultrafast -shortest -acodec copy test_invictus_test.mp4

  }

  public function getAudioParams ($requestParams) {
    $firstPart = $this->audioPath . "first_part.mp3";
    $secondPart = $this->audioPath . "second_part.mp3";
    $music = $this->audioPath . "music.mp3";
    return [
      "full_name_audio_channel" => $firstPart . $this->delimiter . $this->getNameSample($requestParams) . $this->delimiter . $secondPart,
      "music" => $music
    ];
  }

  public function getNameSample ($requestParams) {
    return $this->nameSamplesPath . $requestParams->getName(true) . ".mp3";
  }

  public function getNamesPath () {
    return $this->nameSamplesPath;
  }

  public function getPhotoParams () {
    return [
      "x" => $this->photo_x_shift,
      "y" => $this->photo_y_shift,

      "start_second" => $this->photo_start_second,
      "start_fade_seconds" => $this->photo_start_fade_seconds_count,

      "end_second" => $this->photo_end_second,
      "end_fade_seconds" => $this->photo_end_fade_seconds_count,

      "start_second" => $this->photo_start_second,
      "end_second" => $this->photo_end_second
    ];
  }

  public function getVideoParams () {
    return [
      "sample_path" => $this->videoPath
    ];
  }

}
