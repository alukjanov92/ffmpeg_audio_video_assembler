<?php

class DedMorozParams {

  public function __construct () {
    $this->rootVideoPath = "/home/developer/ffmpeg_audio_video_assembler/media_src/dedmoroz/";

    $this->audioPath = $this->rootVideoPath . "audio/";

    $this->videoPath = $this->rootVideoPath . "video/sample/sample.mp4";

    $this->delimiter = "|";

    $this->musicChannel = $this->audioPath . "music.mp3";

    $this->nameSamplesPath = $this->audioPath . "name_samples/";

    $this->firstSamplePathPart = "first/";
    $this->secondSamplePathPart = "second/";

    $this->photo_x_shift = 1302;
    $this->photo_y_shift = 265;

    $this->photo_start_frame = 1401;
    $this->photo_start_fade_frames_count = 22;

    $this->photo_end_frame = 1714;
    $this->photo_end_fade_frames_count = 32;

    $this->photo_start_second = 56.72;
    $this->photo_end_second = 72.2;

  }

  public function getBoysNames () {
    return $this->nameSamplesPath . "boy/first/";
  }

  public function getGirlsNames () {
    return $this->nameSamplesPath . "girl/second/";
  }

  public function getAudioParams($params){
    $firstPart  = $this->audioPath . $params->getGender() . "/" . "first_part.mp3";
    $secondPart = $this->audioPath . $params->getGender() . "/" . "second_part.mp3";
    $thirdPart  = $this->audioPath . $params->getGender() . "/" . "third_part.mp3";

    return [
      "full_name_audio_channel" => $firstPart . $this->delimiter . $this->getNameSample($params, $this->firstSamplePathPart) . $this->delimiter . $secondPart . $this->delimiter . $this->getNameSample($params, $this->secondSamplePathPart) . $this->delimiter . $thirdPart,
      "music" => $this->musicChannel
    ];
  }

  public function getNameSample ($params, $part) {
    return $this->nameSamplesPath . $params->getGender() . "/" . $part . $params->getName(true) . ".mp3";
  }

  public function getPhotoParams () {
    return [
      "x" => $this->photo_x_shift,
      "y" => $this->photo_y_shift,

      "start_frame" => $this->photo_start_frame,
      "start_fade_frames" => $this->photo_start_fade_frames_count,

      "end_frame" => $this->photo_end_frame,
      "end_fade_frames" => $this->photo_end_fade_frames_count,

      "start_second" => $this->photo_start_second,
      "end_second" => $this->photo_end_second
    ];
  }

  public function getVideoParams () {
    return [
      "sample_path" => $this->videoPath
    ];
  }

}
