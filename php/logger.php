<?php
  define('LOG_FILE_NAME', 'error.log');
  class Logger{
    public function writeLog($token, $msg, $file){
      if(!$file){
        $file = LOG_FILE_NAME;
      }
      file_put_contents($file, "Error serving request with token " . $token . PHP_EOL . $msg . PHP_EOL, FILE_APPEND);
    }

    public function logException($token, $e, $file){
      $msgStart = "========== Exception ==========";
      $msgEnd = "========== End of Exception ==========";
      $msgBody = implode(PHP_EOL, ["Date - " . date("Y-m-d H:i:s"), "Message - " . $e->getMessage(), "Code - " . $e->getCode(), "Trace - " . PHP_EOL . $e->getTraceAsString()]);
      $logMessage = $msgStart . PHP_EOL . $msgBody . PHP_EOL . $msgEnd;
      $this->writeLog($token, $logMessage, $file);
    }

    public function clearLog(){
      unlink(LOG_FILE_NAME);
    }

  }
?>
