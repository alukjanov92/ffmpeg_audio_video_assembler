<?php

ini_set("include_path", __DIR__."/php/");
define("ROOT_PATH", __DIR__."/");
define("TMP", ROOT_PATH . "tmp/");
define("DEBUG", false);
define("DEBUG_FTP", false);
define("DEBUG_VIDEO", false);
define("USE_AMAZON", true);
include "ConfigParams.php";
include "RequestParams.php";
include "ParamsValidator.php";
include "videoprocessor/VideoAssembler.php";
include "FtpManager/FtpManager.php";
include "AWSUploader/AWSUploader.php";
include "siteApi/SiteApiHelper.php";
include "db/OperationLogger.php";
include "logger.php";
include "randomName.php";

$requestParams = new RequestParams();
$configParams = new ConfigParams();
$siteApiHelper = new SiteApiHelper($configParams);
$loger = new Logger();
$operationLogger = new OperationLogger();
$paramsValidator = new ParamsValidator();
$token = $requestParams->getToken();

try {
  $paramsValidator->validateTextVideoRequest($requestParams, $operationLogger);
  $operationLogger->registerOperation($token);
  $siteApiHelper->acceptRequest(['status'=>'accepted', 'token' => $token]);
  if(DEBUG_FTP === false){
    $videoAssembler = new VideoAssembler();
    $resultFile = $videoAssembler->processTextVideo($requestParams);
  }else{
    $resultFile = new ResultFile();
    $resultFile->setFilePath('/home/developer/ffmpeg_audio_video_assembler/tmp/test_upload1.mp4');
  }
  if (DEBUG_VIDEO === false) {
    if (USE_AMAZON === false) {
      $ftpManager = new FtpManager($configParams);
      $ftpManager->uploadVideoToFtp($resultFile);
    } else {
      $awsUploader = new AWSUploader();
      $awsUploader->uploadFileToS3($resultFile);
    }
  }else{
    $operationLogger->updateOperationSuccess($token, $resultFile);
    $siteApiHelper::processFileDownload($resultFile->filePath);
    exit;
  }
  $operationLogger->updateOperationSuccess($token, $resultFile);
  $siteApiHelper->sendOperationSucces($token, $resultFile);
  unlink($resultFile->filePath);
} catch(Exception $e) {
  $msg = $e->getMessage();
  $code = $e->getCode();
  $logPath = null; // default is setted in logger class
  if($e instanceof ParamsException){
    $siteApiHelper->declineRequest($msg, $code);
  }elseif($e instanceof FtpException){
    $operationLogger->updateOperationFail($token, $msg, $code);
    $siteApiHelper->sendOperationFail($token, $msg, $code);
  }elseif($e instanceof DbException){
    $logPath = "db_errors.log";
  }else{
    $operationLogger->updateOperationFail($token);
  }
  $loger->logException($token, $e, $logPath);
}
